<?php

class CurrencyConverter{
    
    public function __construct(){
        $this->client = new Buzz();
        $this->client->getClient->setTimeout(120)
    }
    
    private $endpoint = "https://api.fixer.io/latest?base=%s&symbols=%s";
    private $cache = [];
    
    public function getMultiplier($from, $to){
        
        if(!isset($this->cache[$from]) && !isset($this->cache[$from][$to])){
            // TODO: Use another way to send requet for better errorhandling
            $url = sprintf($this->endpoint, $from, $to);
            $response = file_get_contents($url);
            $json = json_decode($response);
            
            $this->cache[$from][$to] = $json->rates->$to;    
        }
        
        return $this->cache[$from][$to];
    }
    
    public function convert($amount, $from, $to){
        $multiplier = $this->getMultiplier($from, $to);
        return $amount*$multiplier;
    }
}